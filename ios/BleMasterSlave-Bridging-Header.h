#if __has_include(<React/RCTBridgeModule.h>)
    #import <React/RCTBridgeModule.h>
    #import <React/RCTEventDispatcher.h>
    #import <React/RCTEventEmitter.h>
    #import <React/RCTBridge.h>
    #import <React/RCTConvert.h>
#else
    #import "RCTBridgeModule.h"
    #import "RCTEventDispatcher.h"
    #import "RCTEventEmitter.h"
    #import "RCTBridge.h"
    #import "RCTConvert.h"
#endif

#include "RNLBeaconScanner.h"
#include "RNLURLBeaconCompressor.h"
#include "RNLBeacon+Distance.h"

