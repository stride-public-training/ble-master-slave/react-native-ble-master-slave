
@objc(BleMasterSlave)
class BleMasterSlave: RCTEventEmitter {
    

    
    override func supportedEvents() -> [String]! {
        return [
            "BLEMessages","BLEConnectionStatus"
        ]
    }
    
    /*
     Start Beacon service on the iOS Device, the master can send data via bluetooth. To use this features call
     notifySlaves(message: String)
     */
    
    @objc(initializeWithOptions:resolve:reject:)
    func initializeWithOptions(options: Dictionary<String, Any>, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        print(#function)
        BLEManager.shared.setDelegate(delegate: self)
        BLEManager.shared.initializeWithOptions(options: options,resolve: resolve , reject: reject)
    }
    
    @objc(initialize:reject:)
    func initialize(resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        print(#function)
        BLEManager.shared.setDelegate(delegate: self)
        BLEManager.shared.initialize(resolve: resolve , reject: reject)
    }
    
    @objc(disconnectFromService:reject:)
    func disconnectFromService(  resolve: @escaping RCTPromiseResolveBlock, reject:@escaping  RCTPromiseRejectBlock) -> Void {
        print(#function)
        BLEManager.shared.disconnectFromService(resolve: resolve , reject: reject)
    }
    
    @objc(connectToService:reject:)
    func connectToService(  resolve: @escaping RCTPromiseResolveBlock, reject:@escaping  RCTPromiseRejectBlock) -> Void {
        print(#function)
        BLEManager.shared.connectToService(resolve: resolve , reject: reject)
    }
    
}

extension BleMasterSlave : BLEEventsProtocol {
    func onMessageReceived(message: String?) {
        sendEvent(withName: "BLEMessages", body: message)
    }
    
    func onConnectionChanged(status: String) {
        sendEvent(withName: "BLEConnectionStatus", body: status)
    }
    
    
}
