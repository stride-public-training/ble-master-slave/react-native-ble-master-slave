
//
//  BLEManager.swift
//  Pods
//
//  Created by Salif Omar Faye on 14/07/2020.
//

import Foundation
import CoreBluetooth
import CoreLocation

struct DefaultOptions{
    static let UUID_DEFAULT : String = "0000FED8-0000-1000-8000-00805F9B34FB"
    static let UUID_SERVICE : String = "00001111-0000-1000-8000-00805F9B34FB"
    static let UUID_MESSAGE : String = "00002222-0000-1000-8000-00805F9B34FB"
    static let UUID_DESCRIPTOR : String = "00003333-0000-1000-8000-00805F9B34FB"
    static let SCAN_TIME : Int64 = 10
    static let DEVICE_NAME : String = "DuvaMasterDevice"
}

struct BLEOptions{
    var serviceUUID: String;
    var messageUUID: String;
    var defaultUUID: String;
    var masterName: String;
    var scanTime: Int64;
}

protocol BLEManagerProtocol{
    func initializeWithOptions(options: Dictionary<String, Any>, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock)
    func initialize( resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock)
    func connectToService( resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock)
    func disconnectFromService(resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock)
}

protocol BLEEventsProtocol {
    func onMessageReceived(message: String?)
    func onConnectionChanged(status: String)
}

class BLEManager: NSObject,BLEManagerProtocol{
    
    var defaultOptions: BLEOptions = BLEOptions(serviceUUID: DefaultOptions.UUID_SERVICE, messageUUID: DefaultOptions.UUID_MESSAGE, defaultUUID: DefaultOptions.UUID_DEFAULT, masterName: DefaultOptions.DEVICE_NAME, scanTime: DefaultOptions.SCAN_TIME)
    
    var delegate: BLEEventsProtocol!
    
    static var shared = BLEManager()
    private var centralManager: CBCentralManager!
    private var scanningTimer: Timer?
    var connectedPeripheral: CBPeripheral?
    var targetService: CBService?
    var writableCharacteristic: CBCharacteristic?

    var isConnected: Bool = false {
        didSet{
            if !isConnected { self.startScanning() }
        }
    }

    
    override init() {
        print("Initialization ...")
    }
    
    func setDelegate(delegate: BLEEventsProtocol){
        self.delegate = delegate
    }
    
    // - BLEManagerProtocol
    func initializeWithOptions(options: Dictionary<String, Any>, resolve: (Any?) -> Void, reject: (String?, String?, Error?) -> Void) {
        print(#function)
        #warning("Inizializzare defaultOptions con options")
        self.initialize(resolve: resolve, reject: reject)
        resolve(true)

    }
    
    func initialize(resolve: (Any?) -> Void, reject: (String?, String?, Error?) -> Void) {
        print(#function)
        centralManager = CBCentralManager(delegate: self, queue: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.startScanning()
        }
        resolve(true)
    }
    
    func connectToService(resolve: (Any?) -> Void, reject: (String?, String?, Error?) -> Void) {
        print(#function)
        self.startScanning()
        resolve(true)

    }
    
    func disconnectFromService(resolve: (Any?) -> Void, reject: (String?, String?, Error?) -> Void) {
        print(#function)
        self.disconnectPeripheral()
        resolve(true)

    }
    
    func writeValue(value: Int8) {
        guard let peripheral = connectedPeripheral, let characteristic = writableCharacteristic else {
            return
        }

        #warning("Da finire se master -> iOS ")
        //let data = Data.dataWithValue(value: value)
        //peripheral.writeValue(data, for: characteristic, type: .withResponse)
    }

    
}

// - BLE Management

extension BLEManager : CBCentralManagerDelegate {
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.discoverServices(nil)
        self.delegate.onConnectionChanged(status: "slave:connected")
        self.isConnected = true
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected")
        self.delegate.onConnectionChanged(status: "slave:disconnected")
        self.isConnected = false
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        connectedPeripheral = peripheral
        
        if let connectedPeripheral = connectedPeripheral {
            connectedPeripheral.delegate = self
            centralManager.connect(connectedPeripheral, options: nil)
        }
        centralManager.stopScan()
    }
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if centralManager.state == .poweredOn {
            //startScanning()
        }
    }
    

    /**
     Start peripheral search
     */
    func startScanning() {
        if scanningTimer != nil {
            scanningTimer?.invalidate()
            scanningTimer = nil
        }
        scanningTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(defaultOptions.scanTime), repeats: false, block: { (_) in
            self.stopScanning()
            self.isConnected = false

        })
        self.centralManager.scanForPeripherals(withServices: [ CBUUID(nsuuid: UUID(uuidString: defaultOptions.defaultUUID)!) ], options: nil)
    }
    
    /**
     Stop peripheral search
     */
    func stopScanning() {
        self.centralManager.stopScan()
    }
    
    func disconnectPeripheral(){
        guard let peripheral = self.connectedPeripheral else {
            return
        }
        self.centralManager.cancelPeripheralConnection(peripheral)
    }
}



extension BLEManager: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {

        NSLog("didDiscoverServices");
        guard let services = peripheral.services else {
            return
        }

        targetService = services.first
        if let service = services.first {
            targetService = service
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {

        NSLog("didDiscoverCharacteristicsFor");
        guard let characteristics = service.characteristics else {
            return
        }

        for characteristic in characteristics {
            if characteristic.properties.contains(.write) || characteristic.properties.contains(.writeWithoutResponse) {
                writableCharacteristic = characteristic
            }
            peripheral.setNotifyValue(true, for: characteristic)
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {

        NSLog("didUpdateValueFor");
        guard let data = characteristic.value else {
            return
        }
        let receivedData: String? = String(decoding: data, as: UTF8.self)
        print(receivedData)
        self.delegate.onMessageReceived(message: receivedData)
    }
}
