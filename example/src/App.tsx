import * as React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import BleMasterSlave from 'react-native-ble-master-slave';

interface State {
  connected: boolean;
  masterUUID: string;
  messageToSend?: string;
  recivedMessages?: string[];
}
export default class App extends React.Component {
  state: State = {
    connected: false,
    masterUUID: 'F651C4C7-B4BC-4656-B246-AF8B709EF297',
    messageToSend: undefined,
    recivedMessages: [],
  };

  componentDidMount = () => {
    /**** Inizializzazione ed eventi  */
    BleMasterSlave.initialize();

    /*
    Può essere inizializzato con dei valori differenti 

    BleMasterSlave.initialize({
      defaultUUID : "0000FED8-0000-1000-8000-00805F9B34FB",
      serviceUUID : "00001111-0000-1000-8000-00805F9B34FB",
      messageUUID : "00002222-0000-1000-8000-00805F9B34FB",
      masterName : "DuvaMasterDevice",
      desUUID : "00003333-0000-1000-8000-00805F9B34FB",
      scanTime : 10, ( secondi )
    });


    */
    BleMasterSlave.setOnConnectionStatusChanged(this.onMessageReceived);
    BleMasterSlave.setOnMessageReceived(this.onMessageReceived);
  };

  onMessageReceived = (status: string) => {
    this.setState({
      connected: true,
      recivedMessages: this.state.recivedMessages!.push(status),
    });
  };

  startBLEService = async () => {
    try {
      //**** Avvia il Master */
      await BleMasterSlave.startService();
      const message: string = `Started Master with uuid:${this.state.masterUUID}`;
      this.setState({
        connected: true,
        recivedMessages: this.state.recivedMessages!.push(message),
      });
    } catch (e) {
      console.log(e);
    }
  };

  stopBLEService = async () => {
    try {
      //**** Uccide il Master */

      await BleMasterSlave.stopService();
      this.setState({
        connected: false,
        recivedMessages: this.state.recivedMessages!.concat(
          `Stopped Master with uuid:${this.state.masterUUID}`
        ),
      });
    } catch (e) {
      console.log(e);
    }
  };

  searchDevices = async () => {
    try {
      //**** Connessione al Master */
      await BleMasterSlave.connectToService();
    } catch (e) {
      console.log(e);
    }
  };

  notifyMessage = async () => {
    if (this.state.messageToSend != undefined) {
      await BleMasterSlave.notifyMessage(this.state.messageToSend);
    } else {
      Alert.alert('Inserire un messaggio');
    }
  };

  onConnectionChange = (status: boolean) =>
    this.setState({ connected: status });

  renderButton = (title: string, onPress: () => void) => {
    return (
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            height: 100,
            borderRadius: 15,
            backgroundColor: '#bbbbbb',
            margin: 16,
            justifyContent: 'center',
            padding: 8,
          }}
        >
          <Text
            style={{
              fontWeight: '200',
            }}
          >
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render = () => {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            top: 0,
            paddingTop: 40,
            position: 'absolute',
            paddingBottom: 20,
            justifyContent: 'center',
            width: '100%',
            backgroundColor: '#eee',
          }}
        >
          <Text style={{ fontWeight: '200', fontSize: 20 }}>
            {'BLE Master-Slave'}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            backgroundColor: this.state.connected ? 'green' : 'red',
            top: 0,
            height: 22,
            position: 'absolute',
          }}
        ></View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          {this.renderButton('Start Service', this.startBLEService)}
          {this.renderButton('Stop Service', this.stopBLEService)}
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          {this.renderButton('Connect/Search', this.searchDevices)}
          {this.renderButton('Notify', this.notifyMessage)}
        </View>

        <TextInput
          style={{ width: '90%', height: 50, backgroundColor: '#eee' }}
          placeholder={'Inserire messaggio e premere Notify'}
          onChangeText={(it) => this.setState({ messageToSend: it })}
        ></TextInput>

        <View
          style={{
            bottom: 0,
            height: 200,
            backgroundColor: '#eee',
            width: '100%',
            padding: 8,
            position: 'absolute',
            flexDirection: 'column',
          }}
        >
          <Text style={{ fontWeight: '200', fontSize: 20 }}>Messages</Text>
          <ScrollView>
            {this.state.recivedMessages!.map((it) => (
              <Text>{it}</Text>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
