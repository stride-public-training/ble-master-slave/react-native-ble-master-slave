package com.reactnativeblemasterslave.core.notifications

import android.content.Context
import android.util.Log
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.reactnativeblemasterslave.BLEManager
import com.reactnativeblemasterslave.core.master.MasterGattManager
import com.reactnativeblemasterslave.core.notifications.interfaces.NotificationGattDelegate

class NotificationManager private constructor(context: Context) : NotificationGattDelegate {

  private val TAG: String = "NotificationGattManager"

  private val EVENT_MESSAGE: String = "BLEMessages"
  private val EVENT_CONNECTION: String = "BLEConnectionStatus"


  /** Single instance of MasterGattManager **/
  companion object { // Singleton, single MasterGattManager instance
    @Volatile
    private var _instance: NotificationManager? = null
    private lateinit var _context: Context

    fun instance(context: Context): NotificationManager {
      return when {
        _instance != null -> _instance!!
        else -> synchronized(this) {
          if (_instance == null) _instance = NotificationManager(context)
          _instance!!
        }
      }
    }
  }

  override fun sendMessage(message: String?) {
    Log.e("$TAG", message)
    if (message != null) MasterGattManager.instance(_context).notifyRegisteredDevices(message)
  }

  override fun sendNotification(message: String) {
    Log.e("$TAG", message)
    BLEManager.context!!
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
      .emit(EVENT_MESSAGE, message)
  }

  override fun sendConnectionMessage(message: String) {
    Log.e("$TAG", message)
    BLEManager.context!!
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
      .emit(EVENT_CONNECTION, message)
  }

}
