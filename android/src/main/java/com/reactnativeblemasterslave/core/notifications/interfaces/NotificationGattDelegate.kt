package com.reactnativeblemasterslave.core.notifications.interfaces

interface NotificationGattDelegate {
  fun sendMessage(message: String?)
  fun sendNotification(message: String)
  fun sendConnectionMessage(message: String)
}
