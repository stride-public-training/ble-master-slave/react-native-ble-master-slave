package com.reactnativeblemasterslave.core.master

import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.os.ParcelUuid
import com.reactnativeblemasterslave.BLEManager
import com.reactnativeblemasterslave.core.master.interfaces.MasterGattDelegate
import com.reactnativeblemasterslave.core.notifications.NotificationManager

class MasterGattManager private constructor(context: Context) : MasterGattDelegate {

  /** Single instance of MasterGattManager **/
  companion object { // Singleton, single MasterGattManager instance
    @Volatile
    private var _instance: MasterGattManager? = null
    private lateinit var _context: Context
    private var server: BluetoothGattServer? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothLeAdvertiser: BluetoothLeAdvertiser? = null
    private var start = false

    fun instance(context: Context): MasterGattManager {
      return when {
        _instance != null -> _instance!!
        else -> synchronized(this) {
          if (_instance == null) _instance = MasterGattManager(context)
          _instance!!
        }
      }
    }
  }

  override fun setupBluetooth() {
    val bluetoothManager = _context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    server = bluetoothManager.openGattServer(_context, serverCallback)
    bluetoothAdapter = bluetoothManager.adapter
    initServer()
    advertise()
  }

  override fun initServer() {
    val service = BluetoothGattService(ParcelUuid.fromString(BLEManager.options!!.serviceUUID).uuid, BluetoothGattService.SERVICE_TYPE_PRIMARY)
    val characteristic = BluetoothGattCharacteristic(ParcelUuid.fromString(BLEManager.options!!.messageUUID).uuid, BluetoothGattCharacteristic.PROPERTY_WRITE or BluetoothGattCharacteristic.PROPERTY_READ, BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE)
    service.addCharacteristic(characteristic)
    server!!.addService(service)
  }

  override fun advertise() {
    bluetoothLeAdvertiser = bluetoothAdapter!!.bluetoothLeAdvertiser
    val advertisementData = advertisementData
    val advertiseSettings = advertiseSettings
    bluetoothLeAdvertiser!!.startAdvertising(advertiseSettings, advertisementData, advertiseCallback)
    start = true
  }

  override fun notifyRegisteredDevices(message: String) {
    val characteristic: BluetoothGattCharacteristic = server!!
      .getService(ParcelUuid.fromString(BLEManager.options!!.serviceUUID).uuid)
      .getCharacteristic(ParcelUuid.fromString(BLEManager.options!!.messageUUID).uuid)
    for (device in BLEManager.connectedDevices()) {
      characteristic!!.value = message.toByteArray()
      server!!.notifyCharacteristicChanged(device, characteristic, false)
    }
  }

  override fun onDestroyAll() {
    if (start) {
      bluetoothLeAdvertiser!!.stopAdvertising(advertiseCallback)
    }
  }

  /**** AdvertiseData ***/

  private val advertisementData: AdvertiseData
    private get() {
      val builder = AdvertiseData.Builder()
      builder.setIncludeTxPowerLevel(true)
      builder.addServiceUuid(ParcelUuid.fromString(BLEManager.options!!.defaultUUID))
      bluetoothAdapter!!.name = BLEManager.options!!.masterName
      builder.setIncludeDeviceName(true)
      return builder.build()
    }

  private val advertiseSettings: AdvertiseSettings
    private get() {
      val builder = AdvertiseSettings.Builder()
      builder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
      builder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
      builder.setConnectable(true)
      return builder.build()
    }


  /*** Callbacks ***/


  private val advertiseCallback: AdvertiseCallback = object : AdvertiseCallback() {
    @SuppressLint("Override")
    override fun onStartSuccess(advertiseSettings: AdvertiseSettings) {
      NotificationManager.instance(_context).sendConnectionMessage("master:connected")
    }

    @SuppressLint("Override")
    override fun onStartFailure(i: Int) {
      NotificationManager.instance(_context).sendConnectionMessage("master:disconnected")
    }
  }

  private val serverCallback: BluetoothGattServerCallback = object : BluetoothGattServerCallback() {
    override fun onConnectionStateChange(device: BluetoothDevice, status: Int, newState: Int) {
      super.onConnectionStateChange(device, status, newState)
      if (newState == BluetoothProfile.STATE_CONNECTED) {
        print("${device.name} - ${device.address} | Client connected ")
        BLEManager.addConnectedDevice(device)
      }
      if (newState == BluetoothProfile.STATE_DISCONNECTED) {
        print("${device.name} - ${device.address} | Client disconnected ")
        BLEManager.removeConnectedDevice(device)
      }
    }

    override fun onCharacteristicWriteRequest(device: BluetoothDevice, requestId: Int, characteristic: BluetoothGattCharacteristic, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray) {
      super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value)
      val message = String(value)
      NotificationManager.instance(_context).sendNotification(message)
      server!!.sendResponse(device, requestId, 0, offset, value)
    }

  }

}
