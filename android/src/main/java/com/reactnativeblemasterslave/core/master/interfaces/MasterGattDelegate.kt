package com.reactnativeblemasterslave.core.master.interfaces

interface MasterGattDelegate {
  fun setupBluetooth()
  fun initServer()
  fun advertise()
  fun notifyRegisteredDevices(message: String)
  fun onDestroyAll()
}

