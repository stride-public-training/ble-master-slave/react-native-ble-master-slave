package com.reactnativeblemasterslave.utils

import android.bluetooth.*
import android.os.ParcelUuid
import android.util.Log
import com.facebook.react.bridge.ReactContext
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.reactnativeblemasterslave.BLEManager


class DeviceConnection {
  private var device: BluetoothDevice? = null
  private var services: List<BluetoothGattService>? = null
  private var currentGatt: BluetoothGatt? = null
  private var context: ReactContext? = null
  private var characteristic: BluetoothGattCharacteristic? = null

  fun initialize(context: ReactContext, currentDevice: BluetoothDevice) {
    //if (this.device == null) {
    this.device = currentDevice
    this.context = context
    this.currentGatt = this.device!!.connectGatt(this.context!!, false, gattCallback)
    //this.currentGatt!!.disconnect()


    //} else {
    // Log.e("Connection", "Already connected, try disconnection before retry.")
    // }
  }

  fun disconnect(){
    if(this.currentGatt != null){
      this.currentGatt!!.disconnect()
    }
  }

  fun sendMessage(message: String) {

    //Single 1 - 1
    if (services.isNullOrEmpty()) return
    val notificationServices = services!!.filter { it -> it.uuid == (ParcelUuid.fromString(BLEManager.options!!.serviceUUID).uuid) }
    if (notificationServices.isEmpty()) {
      return
    }

    notificationServices.forEach {
      characteristic = it.getCharacteristic(ParcelUuid.fromString(BLEManager.options!!.messageUUID).uuid)
      if (characteristic != null && currentGatt != null) {
        characteristic!!.value = message.toByteArray()
        currentGatt!!.writeCharacteristic(characteristic)
      }
    }

  }

  /**** ****/

  private var gattCallback: BluetoothGattCallback? = object : BluetoothGattCallback() {
    override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
      super.onCharacteristicChanged(gatt, characteristic)
      if (ParcelUuid.fromString(BLEManager.options!!.messageUUID).uuid == characteristic!!.uuid) {
        val value: String = String(characteristic.value)
        BLEManager.context!!.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit("BLEMessages", value)
      }
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
      super.onConnectionStateChange(gatt, status, newState)
      val isConnected = newState == BluetoothProfile.STATE_CONNECTED
      if (BLEManager.masterAddress != gatt.device.address || isConnected != BLEManager.masterConnected ) {
        if (newState == BluetoothProfile.STATE_CONNECTED) {
          BLEManager.masterAddress = gatt.device.address
          BLEManager.masterConnected = true
          currentGatt!!.discoverServices()
          BLEManager.context!!.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit("BLEConnectionStatus", "slave:connected")
        } else {
          BLEManager.masterAddress = gatt.device.address
          BLEManager.masterConnected = false
          BLEManager.context!!.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit("BLEConnectionStatus", "slave:disconnected")
        }
      }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
      super.onServicesDiscovered(gatt, status)
      services = currentGatt!!.services

      val characteristic = gatt
        .getService(ParcelUuid.fromString(BLEManager.options!!.serviceUUID).uuid)
        .getCharacteristic(ParcelUuid.fromString(BLEManager.options!!.messageUUID).uuid)

      gatt.setCharacteristicNotification(characteristic, true)

      val descriptor = characteristic.getDescriptor(ParcelUuid.fromString(BLEManager.options!!.desUUID).uuid)
      descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
      gatt.writeDescriptor(descriptor)
    }

    override fun onCharacteristicWrite(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int) {
      super.onCharacteristicWrite(gatt, characteristic, status)
      gatt.executeReliableWrite()
    }

  }


}
