package com.reactnativeblemasterslave.utils

import com.facebook.react.bridge.ReadableMap

class BLEOptions {
  var serviceUUID: String? = null
  var messageUUID: String? = null
  var defaultUUID: String? = null
  var masterName: String? = null
  var scanTime: Int? = null
  var desUUID: String? = null

  companion object {
    fun fromReadableMap(map: ReadableMap): BLEOptions {
      var options: BLEOptions = BLEOptions()
      options.defaultUUID = if (map.hasKey("defaultUUID")) map.getString("defaultUUID") else defaultOptions().defaultUUID
      options.serviceUUID = if (map.hasKey("serviceUUID")) map.getString("serviceUUID") else defaultOptions().serviceUUID
      options.messageUUID = if (map.hasKey("messageUUID")) map.getString("messageUUID") else defaultOptions().messageUUID
      options.masterName = if (map.hasKey("masterName")) map.getString("masterName") else defaultOptions().masterName
      options.desUUID = if (map.hasKey("desUUID")) map.getString("desUUID") else defaultOptions().desUUID
      options.scanTime = if (map.hasKey("scanTime")) map.getInt("scanTime") else defaultOptions().scanTime
      return options
    }

    fun defaultOptions(): BLEOptions {
      var defaultOption: BLEOptions = BLEOptions()
      defaultOption.defaultUUID = "0000FED7-0000-1000-8000-00805F9B34FB"
      defaultOption.serviceUUID = "00001111-0000-1000-8000-00805F9B34FB"
      defaultOption.messageUUID = "00002222-0000-1000-8000-00805F9B34FB"
      defaultOption.masterName = "DuvaMasterDevice"
      defaultOption.desUUID = "00003333-0000-1000-8000-00805F9B34FB"
      defaultOption.scanTime = 10
      return defaultOption
    }
  }
}

/*
object Helper {
  fun checkBLE(context: Context): Boolean {
    return context.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
  }

  fun getServiceName(uuid: String?): String {
    return when (uuid) {
      "00001800-0000-1000-8000-00805f9b34fb" -> {
        "Generic Access"
      }
      "00001801-0000-1000-8000-00805f9b34fb" -> {
        "Generic Attribute"
      }
      "00001111-0000-1000-8000-00805f9b34fb" -> {
        "Notification Service"
      }
      else -> "Unknown Service"
    }
  }
}

 */
