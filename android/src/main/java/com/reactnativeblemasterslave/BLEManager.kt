package com.reactnativeblemasterslave

import android.app.ActivityManager
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.ParcelUuid
import android.util.Log
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactContext
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.reactnativeblemasterslave.core.notifications.NotificationManager
import com.reactnativeblemasterslave.utils.BLEOptions
import com.reactnativeblemasterslave.utils.DeviceConnection
import com.reactnativeblemasterslave.services.GattService
import java.util.*
import kotlin.properties.Delegates


object BLEManager {

  private var connection: DeviceConnection = DeviceConnection()
  private var connectedDevices: MutableList<BluetoothDevice> = arrayListOf()
  private const val REQUEST_ENABLE_BT = 1
  private var bluetoothAdapter: BluetoothAdapter? = null
  private var scanner: BluetoothLeScanner? = null
  private var handler: Handler? = null
  private var scanHandler: Handler? = null

  var context: ReactContext? = null
  var options: BLEOptions? = BLEOptions.defaultOptions()
  var masterAddress: String? = null

  var masterConnected: Boolean by Delegates.observable(false) { property, oldValue, newValue ->
    print("Changed -> $newValue")
    if (!newValue) {
      this.connectToService(null)
    }
  }

  /**
   * Initialization
   */
  fun initialize(reactContext: ReactContext) {
    this.context = reactContext
    this.handler = Handler()
  }

  /**
   * Initialization with options
   * options: BLEOptions
   */
  fun initializeWithOptions(options: BLEOptions, reactContext: ReactContext) {
    this.options = options
    this.context = reactContext
    this.handler = Handler()
  }

  /**
   * Start Master service
   * @refactoring -> MasterManager
   */
  fun startService(promise: Promise) {
    Log.d("BLEMANAGER", "Start BLE Service...")
    val bluetoothManager = this.context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    val bluetoothAdapter = bluetoothManager.adapter
    if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) {
      bluetoothAdapter.enable()
      promise.reject("BLEManager:startService -> Error on service start");
      return
    }
    // Start service
    if (!this.isGATTServiceRunning()) {
      this.context!!.startService(Intent(this.context!!, GattService::class.java))
    } else {
      context!!.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit("BLEConnectionStatus", "master:connected")
    }
    promise.resolve(true)
  }


  /**
   * Stop Master service
   * @refactoring -> MasterManager
   */
  fun stopService(promise: Promise) {
    Log.d("BLEMANAGER", "Stop BLE Service...")
    if (this.isGATTServiceRunning()) {
      this.context!!.stopService(Intent(this.context, GattService::class.java))
      val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
      if (mBluetoothAdapter.isEnabled) {
        mBluetoothAdapter.disable()
      }
      if (!this.isGATTServiceRunning()) {
        context!!.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit("BLEConnectionStatus", "master:disconnected")
      }
    }
    promise.resolve(true)
  }

  /**
   * Send messages to connected slaves
   * @refactoring -> MasterManager
   */
  fun notifyMessage(message: String?) {
    context?.let { NotificationManager.instance(it).sendMessage(message) }
  }


  fun reloadScanner() {
    val bluetoothManager = this.context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    bluetoothAdapter = bluetoothManager.adapter
    if (bluetoothAdapter == null || !bluetoothAdapter!!.isEnabled) {
      val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
      this.context!!.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT, null)
    }
    scanner = bluetoothAdapter!!.bluetoothLeScanner
  }

  fun connectToService(promise: Promise?) {
    startScannLEDevices()
    promise?.resolve(true)
  }

  /**
   *  Check if the bluetooth service is running
   */
  private fun isGATTServiceRunning(): Boolean {
    val serviceClass: Class<*> = GattService::class.java
    val manager = context!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
      if (serviceClass.name == service.service.className) {
        return true
      }
    }
    return false
  }

  /**
   * Start bluetooth scanning for BLE
   */
  private fun startScannLEDevices() {
    val filter = ScanFilter.Builder()
    filter.setServiceUuid(ParcelUuid.fromString(options!!.defaultUUID))
    val filters: MutableList<ScanFilter> = ArrayList()
    filters.add(filter.build())

    this.reloadScanner()
    if (scanHandler == null)
      scanHandler = Handler()
    scanHandler!!.postDelayed({
      if (BLEManager.masterConnected) {
        scanner?.stopScan(scanCallback)
      } else {
        scanHandler!!.removeCallbacksAndMessages(null)
        scanHandler = null
        this.startScannLEDevices()
      }
      //BLEManager.masterConnected = false
    }, (options!!.scanTime!! * 1000).toLong())
    //scanner.startScan(scanCallback);
    scanner?.startScan(filters, ScanSettings.Builder().build(), scanCallback)

  }


  /****** Connected Devices ****/

  fun disconnectDevice(promise: Promise) {
    connection.disconnect()
    promise.resolve(true)
  }

  fun addConnectedDevice(device: BluetoothDevice) {
    //if (this.connectedDevices.indexOfFirst { it.address == device.address } == null)
    this.connectedDevices.add(device)
  }

  fun connectedDevices(): List<BluetoothDevice> {
    return this.connectedDevices
  }

  fun removeAllConnectedDevices() {
    this.connectedDevices.clear()
  }

  fun removeConnectedDevice(device: BluetoothDevice) {
    this.connectedDevices.removeAt(this.connectedDevices.indexOfFirst { it.address == device.address })
  }


  /***************/
  private fun isServiceRunning(name: String): Boolean {
    val manager = this.context!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Int.MAX_VALUE)) {
      if (name == service.service.className) {
        return true
      }
    }
    return false
  }

  private val scanCallback: ScanCallback = object : ScanCallback() {
    override fun onScanResult(callbackType: Int, result: ScanResult) {
      super.onScanResult(callbackType, result)
      handler!!.post {
        val device = result.device
        connection.initialize(context!!, device)
        Log.e("Devices", device.name)
      }
    }

    override fun onScanFailed(errorCode: Int) {
      print("Scan failed")
      //this.startScannLEDevices(true)
      super.onScanFailed(errorCode)
    }
  }

}
