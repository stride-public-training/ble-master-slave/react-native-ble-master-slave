package com.reactnativeblemasterslave

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat.requestPermissions
import com.facebook.react.bridge.*
import com.reactnativeblemasterslave.utils.BLEOptions
import java.util.*


class BleMasterSlaveModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {


  override fun getName(): String {
    return "BleMasterSlave"
  }

  private fun hasPermissions(): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (reactApplicationContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !== PackageManager.PERMISSION_GRANTED) {
        requestPermissions(reactApplicationContext.currentActivity!!, arrayOf<String>(Manifest.permission.ACCESS_COARSE_LOCATION), 1001)
        return false
      }
    }
    return true
  }

  @ReactMethod
  fun initializeWithOptions(options: ReadableMap, promise: Promise) {
    this.hasPermissions()
    BLEManager.initializeWithOptions(BLEOptions.fromReadableMap(options),reactApplicationContext)
    promise.resolve(true)
  }

  @ReactMethod
  fun initialize(promise: Promise) {
    this.hasPermissions()
    BLEManager.initialize(reactApplicationContext)
    promise.resolve(true)
  }

  @ReactMethod
  fun startService(promise: Promise) {
    BLEManager.startService(promise)
    //promise.resolve(true)
  }

  @ReactMethod
  fun stopService(promise: Promise) {
    BLEManager.stopService(promise)
  }

  @ReactMethod
  fun connectToService(promise: Promise) {
    BLEManager.connectToService(promise)
  }

  @ReactMethod
  fun disconnectFromService(promise: Promise) {
    BLEManager.disconnectDevice(promise)
  }

  @ReactMethod
  fun notifyMessage(message: String, promise: Promise) {
    BLEManager.notifyMessage(message)
    promise.resolve(true)
  }


}
