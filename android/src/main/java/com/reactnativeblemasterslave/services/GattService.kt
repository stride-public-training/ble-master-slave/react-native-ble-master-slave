package com.reactnativeblemasterslave.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.reactnativeblemasterslave.core.master.MasterGattManager
import com.reactnativeblemasterslave.core.notifications.interfaces.NotificationGattDelegate
import com.reactnativeblemasterslave.core.notifications.NotificationManager

class GattService : Service(), NotificationGattDelegate {

  override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
    MasterGattManager.instance(this.applicationContext).setupBluetooth()
    return START_STICKY
  }

  override fun onDestroy() {
    MasterGattManager.instance(this.applicationContext).onDestroyAll()
    super.onDestroy()
  }

  override fun onBind(intent: Intent): IBinder? {
    return null
  }

  /**** NotificationDelegate ****/

  override fun sendMessage(message: String?) {
    NotificationManager.instance(this.applicationContext).sendMessage(message)
  }

  override fun sendNotification(message: String) {
    NotificationManager.instance(this.applicationContext).sendNotification(message)
  }

  override fun sendConnectionMessage(message: String) {
    NotificationManager.instance(this.applicationContext).sendConnectionMessage(message)
  }

}
