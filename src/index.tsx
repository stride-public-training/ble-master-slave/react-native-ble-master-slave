import { NativeModules, NativeEventEmitter } from 'react-native';
const BleMaster = NativeModules.BleMasterSlave;
const EventEmitter = new NativeEventEmitter(BleMaster);

type BLEOptions = {
  serviceUUID?: string;
  messageUUID?: string;
  defaultUUID?: string;
  masterName?: string;
  scanTime?: number;
};

type BleMasterSlaveType = {
  initialize(): Promise<boolean>;
  initializeWithOptions(options: BLEOptions): Promise<boolean>;
  startService(): Promise<boolean>;
  stopService(): Promise<boolean>;
  connectToService(): Promise<boolean>;
  notifyMessage(message: string): Promise<boolean>;
};

class BleMasterSlave implements BleMasterSlaveType {
  private onConnectionStatusChangedCallback = (status: string) => {
    console.log("status",status);
  };
  private onMessagesReceivedCallback = (message: string) => {
    console.log("message",message);
  };

  constructor() {
    EventEmitter.addListener('BLEMessages', this.onMessagesReceivedHandler);
    EventEmitter.addListener(
      'BLEConnectionStatus',
      this.onConnectionStatusChangedHandler
    );
  }

  /**** Notifications  */

  private onMessagesReceivedHandler = (message: string) => {
    this.onMessagesReceivedCallback && this.onMessagesReceivedCallback(message);
  };

  private onConnectionStatusChangedHandler = (connectionStatus: string) => {
    this.onConnectionStatusChangedCallback &&
      this.onConnectionStatusChangedCallback(connectionStatus);
  };

  setOnMessageReceived = (callback: (message: string) => void) => {
    this.onMessagesReceivedCallback = callback;
  };

  setOnConnectionStatusChanged = (callback: (status: string) => void) => {
    this.onConnectionStatusChangedCallback = callback;
  };

  /**** Other */

  initializeWithOptions(options: BLEOptions): Promise<boolean> {
    return BleMaster.initializeWithOptions(options);
  }

  disconnectFromService(): Promise<boolean>{
    return BleMaster.disconnectFromService();
  }
  
  initialize(): Promise<boolean> {
    return BleMaster.initialize();
  }
  
  startService(): Promise<boolean> {
    return BleMaster.startService();
  }
  stopService(): Promise<boolean> {
    return BleMaster.stopService();
  }
  connectToService(): Promise<boolean> {
    return BleMaster.connectToService();
  }
  notifyMessage(message: string): Promise<boolean> {
    return BleMaster.notifyMessage(message);
  }
}

export default new BleMasterSlave();
