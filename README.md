# react-native-ble-master-slave

React Native BLE Master slave

## Installation

```sh
npm install react-native-ble-master-slave
```

## Usage

```js
import BleMasterSlave from "react-native-ble-master-slave";

// ...

const result = await BleMasterSlave.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
